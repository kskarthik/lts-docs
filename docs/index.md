---
hide:
---

# Welcome to the Libre Tech Shop Documentation

The Libre Tech Shop is a first of its kind online store for products that
respect your freedom.

Every product you purchase here will be powered by free software, will be
accompanied by documentation and will give you the freedom to hack, learn,
modify and share. These products will not track you and will respect your
freedom and privacy.

# Products

|   Product   | Description                          |
|:-----------:|:------------------------------------:|
| <img class="card-img-top" src="/images/usb/top.png" style="height: 150px; width: auto;"> | [MH-USB](/usb/) <br> The Mostly Harmless USB [MH-USB]  |
| <img class="card-img-top" src="/images/lc230-shadow.png" style="height: 150px; width: auto;"> | [LC230](/lc230/) <br> Liberated Computer [LC230]  |
| <img class="card-img-top" src="/images/phones/lpa2.jpg" style="height: 150px; width: auto;"> | [Phones](/phones/) <br> Liberated Phones [LPA2]  |
| <img class="card-img-top" src="/images/routers/lra7.png" style="height: 150px; width: auto;"> | [Routers](/routers/) <br> Liberated Routers [LRA7]  |

<!--
| <img class="card-img-top" src="/images/mhsw-shadows.png" style="height: 150px; width: auto;"> | [MHSW](/mhsw/) <br> Self-hosted, hackable and private Home Automation MHSW |
| <img class="card-img-top" src="/images/both-rgb-controller.jpg" style="height: 100px; width: auto;"> | [LEDC](/ledc/) <br> LED Lighting Controller LEDC |
-->
