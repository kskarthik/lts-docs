# LC230 - The Liberated Computer

<figure>
  <img src="/images/x230.png"/>
</figure>

## Why LC230?

Why does LC230 exist?

Some of the most fundamental questions that we need to answer about the hardware that we possess are those of "ownership":

!!! question
    - [ ] What is the definition of owning a piece of hardware? 
    - [ ] Are we the owners of the hardware merely because we paid for it and now have possession of it?
    - [ ] If we are owners do we have the necessary freedom and opportunity to do what we want with it?    
    - [ ] How much control does the manufacturer of a device exert after "selling" the device to us?
    - [ ] If such control does exist, then how do we re-define ownership now?

The answers to these questions are not easy, universal or simple. Hence, we might define ownership in the following terms:

!!! info
    We are the true owners of our hardware if we can **do whatever we want** with hardware **without then
    permission of the manufacturer**. This includes the **freedom to change or upgrade hardware components**
    at will, **repair and refurbish** the hardware and **install the software we want** (without constraints).
    
The **"Liberated Computer"** project exists to provide users with convenient access to 
reliable, libre-software friendly and hackable computers. The key to having the freedom to compute in a manner 
we deem respectful is to first liberate the computers from the locks and constraints imposed by the manufacturer.

!!! failure
    <figure>
        <img src="/images/lenovo-error.jpg" />
        <figcaption style="text-align: justify;">
            Even 8 years after the model ceased production, Lenovo's BIOS does not give you the freedom to change the
            wireless card of the laptop to one that would work without proprietary firmware! <br> <br>
            This is a classic example of a manufacturer exerting control over the hardware well beyond its intended lifespan. <br> <br>
            Unless a wireless card that is "white-listed" by the BIOS is plugged in, the system will not boot up. The only way to 
            move beyond this limitation is to replace the BIOS and hence, liberate the computer.
        </figcaption>
    </figure>

## What is Coreboot?

Coreboot is a free/libre software BIOS replacement. It was developed to allow computers users the ability to exert
greater control on their computers by being able to control the BIOS. Coreboot is a very modular BIOS and runs on a wide
variety of computer mainboards. More details are available at: [coreboot.org](https://coreboot.org)

Coreboot's main advantage is that of software freedom at the BIOS level. It liberates the computer and allows a
user to load multiple types of boot payloads while also allowing us to cripple and neuter the Intel ME.

Some of the common payloads supported by Coreboot are:

  - SeaBIOS - the default payload that ships with your LC230
  - Linux - you can embed and load a Linux kernel on the BIOS chip
  - Grub - You can also daisy-chain and boot Grub from Coreboot so that you can perform the rest of the boot process via Grub
  - `nvramtui` - a utility to configure the Embedded Controller (EC) firmware on thinkpad laptops
  - `memtest` - perform memory tests at the BIOS level!
  - `coreinfo` - provides low-level information about Coreboot
  - TINC - play tetris without even booting an OS!

!!! info
    <figure>
        <img src="/images/tinc.png" style="height: auto; width: 400px;"/>
        <figcaption style="text-align: justify;">
            The TINC Coreboot payload - Tetris at the BIOS level. No Operating System!
        </figcaption>
    </figure>

## Introducing Skulls

The [Skulls Project](https://github.com/merge/skulls) is a [Coreboot
distribution](https://doc.coreboot.org/distributions.html). They ship a
pre-compiled Coreboot binary for the Lenovo X230 Thinkpad along with a set of
scripts that make it it simple to install and use it.

The LC230 ships pre-installed with the latest release of Skulls.

## How is an LC230 assembled?

Your LC230 laptop is refurbished and liberated with a mix of new and used components. Here is the process of refurbishing it:

  1. We first purchase a used Lenovo x230 Thinkpad laptop
  2. Once we inspect it for physical and other defects, the process of liberating it starts.
  3. First the laptop is opened up and cleaned - all the internal and external plus the CPU fan.
  4. Next using a SIOC-8 clip, coreboot is flashed to SPI flash that is used to store the system firmware.
  5. Once the sytem is running coreboot, the default Intel wireless card is replaced with an Atheros one.
  6. After that, based on the requested configuration, a new mSATA or SATA SSD is added, more RAM or even a brand new battery.
  7. Finally - an OS is installed and the system is tested for heating issues, networking and so on.

Now your liberated computer is ready for you to play with ! :-) 
  
