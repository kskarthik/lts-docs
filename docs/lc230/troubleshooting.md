# Troubleshooting & Common Problems

## Heating or fan issues

One possible challenge with a refurbished laptop is that there can be heating
issues. All LC230 units that we sell are well tested for such issues to ensure
that the laptop's CPU cools well during heavy CPU usage. This is done by
ensuring two things:

  1. The CPU fan is clean of dust and other foreign material that might affect
     its ability to offer good CPU cooling.

  2. The CPU fan's heatsink is able to conduct heat properly

Check out what a dirty and clean fan looks like.

<div style="text-align: center">
<figure style="display: inline-block" >
  <img src="/images/dirty-fan.jpg" width="250" />
  <figcaption> A dirty fan! </figcaption>
</figure>

&nbsp; &nbsp;

<figure style="display: inline-block" >
  <img src="/images/clean-fan.jpg" width="250" />
  <figcaption> A very clean fan! </figcaption>
</figure>
</div>

If you face any heating issues (or a noisy fan) a few months after purchasing a
LC230, one thing that would help is cleaning the fan using an air blower.

Here is what a clean CPU fan achieves:

<figure style="box-shadow: 0px 0px 20px #888888; margin-bottom: -2em;" >
  <img src="/images/htop.png" />
</figure>

<br>

<figure style="box-shadow: 0px 0px 20px #888888;" >
  <img src="/images/stress-test.png" />
</figure>

As you can see, even under very heavy CPU utilisation (and system load), the
CPU fan running at **5380 RPM** is able to cool down the CPU to keep the core
temperature around +84&#176; C.

## Battery management

In case your current LC230 battery does not give you sufficient backup, you can
purchase a new 6-cell or 9-cell battery. The part numbers are as follows:

  * Lenovo Thinkpad 6-cell Battery 44+ (Part Number: 0A36306)
  * Lenovo ThinkPad 9-cell Battery 44++ (Part Number: 0A36307)

## Swapping the keyboard

It is possible to use the Lenovo Classic keyboard (last seen on the X220
Thinkpad model) with the X230. This involves three steps:

  - Modifying the keyboard connector
  - Flashing a modified BIOS (over the default Lenovo one)
  - Replacing the palm-rest with an x220 one

We will be glad to help you with this modification. The steps are detailed on the 
[Thinkwiki](http://www.thinkwiki.org/wiki/Install_Classic_Keyboard_on_xx30_Series_ThinkPads).

