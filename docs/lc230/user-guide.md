# (LC230) User Guide

## The Coreboot Boot Process

Here is what the boot process looks like with Coreboot installed:

<figure>
  <img src="/images/coreboot-diagram.png" width="420" />
  <figcaption>Coreboot Sequence Diagram</figcaption>
</figure>

The first part of the boot process (labelled as "Coreboot Internal" in the diagram above) is
largely hidden from the user. The boot process is illustrated below:

<figure>
  <img src="/images/lc230-boot.gif" width="420" />
  <figcaption>LC230 Boot Process</figcaption>
</figure>

## Installing an OS

To install any GNU/Linux OS, here is a sequence of steps:

### Build USB Installer

Any GNU/Linux OS would work very well on your LC230 laptop. (Some owners have
also installed and used FreeBSD on it.)

LC230 is configured to run without any proprietary firmware and hence, any of
the following operating systems would work well by default:

  * [Debian GNU/Linux (main)](https://debian.org)
  * [PureOS](https://pureos.net/)
  * [Triquel](https://trisquel.info/)

!!! tip
    
    The [Rasperrby Pi Imager](https://www.raspberrypi.org/software/) is a good
    tool to use to write an OS image to a USB disk. [Balena Etcher](https://www.balena.io/etcher/) is another
    one.

### Plug-in USB Disk

The USB needs to be plugged in before you reboot the laptop - or else SeaBIOS
will not detect it and show it as a boot option.

!!! info

    The blue USB ports on the left side of the laptop are USB3 ports. If you
    have a USB disk that works with USB3, then using these ports will give you
    much better USB disk performance.

    The USB port on the right side (yellow in colour) is a USB2 port.

### Choose the boot device

Press ++escape++ on boot and choose the USB disk as the boot device

<figure>
  <img src="/images/lc230-boot-logo.jpg" width="400" />
  <figcaption>LC230 Boot Image</figcaption>
</figure>

By default, the first boot option is chosen to boot off. This is usually the
2.5" SATA disk if it is plugged in to the laptop. To choose another boot
device, you need to press ++escape++ and then choose the desired boot device
from the SeaBIOS menu.

If you have a 2.5" SATA disk, then a USB disk would be the second boot device.
If you have a 2.5" SATA disk **and** a mSATA disk, then the USB disk would be
the third boot device.

<figure>
  <img src="/images/lc230-usb3.jpg"/>
  <figcaption>LC230 USB3 Ports</figcaption>
</figure>

### Install the OS

You can now proceed with the OS installation as normal.

The LC230 does not provide a UEFI BIOS (like the default Lenovo BIOS firmware).
SeaBIOS is a pc-bios payload and most GNU/Linux OS installers will detect that
and install the correct bootloader. This would be `grub-pc` if you choose to
use Grub and Debian.

## Coreboot Internal Flashing

To flash Coreboot to the x230 motherboard, the first step is to flash the BIOS
externally - ie. using a SIOC-8 flashing clip connecting to the BIOS chips:

<figure>
  <img src="/images/x230-flashing.png"/>
  <figcaption>Liberating the LC230 via External Flashing</figcaption>
</figure>

However, as a part of the external flashing process, the BIOS is unlocked. This
means that you can update / modify the BIOS on the SPI Flash internally - from
within the operating system. Here are some reasons to do that and the steps
involved.

### Configure the kernel

The first things to do before you can flash Coreboot internally, is to boot the
kernel with an additional option:

    iomem=relaxed

On Debian (and derived operating systems such as Ubuntu), you can make this a
permanent setting:

  1. Edit the file: `/etc/default/grub`
  2. Update the line `GRUB_CMDLINE_LINUX` to read:

     `GRUB_CMDLINE_LINUX="iomem=relaxed"`

  3. Run: `update-grub` to update the Grub configuration.

### Query Coreboot details

You can verify the coreboot version (as well as the fact that you're running
coreboot) by using the `dmidecode` utility as follows:

#### BIOS version

``` console
$ sudo dmidecode -t bios
# dmidecode 3.3
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0000, DMI type 0, 26 bytes
BIOS Information
        Vendor: coreboot
        Version: CBET4000 4.10-1082-g4357a82076-dirty
        Release Date: 10/20/2019
        ROM Size: 12 MB
        Characteristics:
                PCI is supported
                PC Card (PCMCIA) is supported
                BIOS is upgradeable
                Selectable boot is supported
                ACPI is supported
                Targeted content distribution is supported
        BIOS Revision: 4.10
        Firmware Revision: 0.0
```

#### System type & name

``` shell-session
$ sudo dmidecode -t system   
# dmidecode 3.3
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0001, DMI type 1, 27 bytes
System Information
        Manufacturer: LENOVO
        Product Name: 2325BU3
        Version: ThinkPad X230
        Serial Number: PK1HM8E
        UUID: ce136601-xxxx-xxxx-xxxx-xxxxxxxxxxxx
        Wake-up Type: Reserved
        SKU Number: Not Specified
        Family: ThinkPad X230

Handle 0x000B, DMI type 32, 11 bytes
System Boot Information
        Status: No errors detected
```

### Install: `flashrom`

The primary tool for operating on your BIOS is called `flashrom`. The Debian
package is also called by the same name and can be installed as:

    apt-get install flashrom

If the kernel is booted correctly with `iomem=relaxed`, you can run `flashrom`:

    flashrom -pinternal

Ouput:

``` shell-session
$ sudo flashrom -pinternal
flashrom v1.2 on Linux 5.10.9-gnu (x86_64)
flashrom is free software, get the source code at https://flashrom.org

Using clock_gettime for delay loops (clk_id: 1, resolution: 1ns).
coreboot table found at 0xbff51000.
Found chipset "Intel QM77".
Enabling flash write... SPI Configuration is locked down.
Enabling hardware sequencing due to multiple flash chips detected.
OK.
Found Programmer flash chip "Opaque flash chip" (12288 kB, Programmer-specific) mapped at physical address 0x0000000000000000.
No operations were specified.
```

!!! tip

    Sometimes `flashrom` will not detect the internal flash chip, though it
    might very well be able to confirm the BIOS type and machine name. In such
    cases, you can try to run `flashrom` as: `flashrom
    -pinternal:laptop=force_I_want_a_brick`. This is not completely safe

### Read BIOS Image

You can dump the BIOS to a file:

``` console
$ flashrom -pinternal -r lc230-coreboot.rom
```

This will read the BIOS to a file: `lc230-coreboot.rom`. This file will be 12 MB in size.

``` console
$ sudo flashrom -pinternal -r lc230.rom
flashrom v1.2 on Linux 5.10.9-gnu (x86_64)
flashrom is free software, get the source code at https://flashrom.org

Using clock_gettime for delay loops (clk_id: 1, resolution: 1ns).
coreboot table found at 0xbff51000.
Found chipset "Intel QM77".
Enabling flash write... SPI Configuration is locked down.
Enabling hardware sequencing due to multiple flash chips detected.
OK.
Found Programmer flash chip "Opaque flash chip" (12288 kB, Programmer-specific) mapped at physical address 0x0000000000000000.
Reading flash... done.
$ ls -lh lc230-coreboot.rom
-rw-r--r-- 1 root root 12M Feb  8 04:46 lc230-coreboot.rom
```

### Write BIOS Image

You can now write back to the BIOS chips as follows:

``` console
$ sudo flashrom -pinternal -w lc230-coreboot.rom
flashrom v1.2 on Linux 5.10.9-gnu (x86_64)
flashrom is free software, get the source code at https://flashrom.org

Using clock_gettime for delay loops (clk_id: 1, resolution: 1ns).
coreboot table found at 0xbff51000.
Found chipset "Intel QM77".
Enabling flash write... SPI Configuration is locked down.
Enabling hardware sequencing due to multiple flash chips detected.
OK.
Found Programmer flash chip "Opaque flash chip" (12288 kB, Programmer-specific) mapped at physical address 0x0000000000000000.
Reading old flash chip contents... done.
Erasing and writing flash chip... 
Warning: Chip content is identical to the requested image.
Erase/write done
```

### Flash: Skulls

The Skulls Coreboot distribution provides built in scripts to upgrade the BIOS.
The update procedure is [detailed
here](https://github.com/merge/skulls/blob/master/x230/README.md#updating).
Your LC230 ships with the latest release of Skull available at the time of
shipping the laptop to you. You can upgrade skulls using the instructions above. 

### Flash: Heads

If you want to experiment with the [Heads firmware](https://osresearch.net/),
instructions are available on the [Heads website](https://osresearch.net/) as
well as in the [Skulls documentation](https://github.com/merge/skulls/blob/master/x230/README.md#moving-to-heads).

## Configuring Coreboot

Your LC230 ships with a SeaBIOS payload called `nvramcui` that allows you to
configure coreboot behaviour and save the settings to nvram.

<div style="text-align: center">
  <figure style="display: inline-block" >
    <img src="/images/nvramcui-1.jpg" width="250" />
    <figcaption> nvramcui options (1) </figcaption>
  </figure>

  <figure style="display: inline-block" >
    <img src="/images/nvramcui-2.jpg" width="235" />
    <figcaption> nvramcui options (2) </figcaption>
  </figure>

  <figure style="display: inline-block" >
    <img src="/images/nvramcui-3.jpg" width="235" />
    <figcaption> nvramcui options (3) </figcaption>
  </figure>
</div>

### Swapping ++fn++ and ++control++

The `fn_ctrl_swap` on the 2nd page of the `nvramcui` settings page enables you
to toggle the ++fn++ and ++control++ swap. If you like to have the control key
on the bottom left corner of the keyboard, then you can set this option to `Enable`.

### Increasing Graphics Memory 

The x230 GPU does not have dedication RAM. Instead it shares its memory with
the main system memory. You can configure how much RAM is dedicated to the GPU
via the `gfx_uma_size` option on the 3rd page of the `nvramcui` settings.

Pressing the ++left++ and ++right++ arrow keys you can iterate over all the
available settings for this option.

### Other settings

You can also configure the following behaviour via nvramcui:

  * **Always On USB**: By enabling this option, you can configure the USB ports
    to be *always on* - even when the laptop is suspended.

  * **Enable/Disable Trackpoing, Touchpad**: You can toggle the `touchpad` and
    `trackpoint` options to enable or disable the the touchpad and trackpoint
    respectively.

