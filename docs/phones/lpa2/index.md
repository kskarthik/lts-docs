# Liberated Phone - LPA2

<div style="text-align: center">
<figure>
  <img src="/images/phones/lpa2.jpg" width="420" />
  <figcaption>
    Use MORE Free Software on your phone. Opt-out of proprietary
    software. Eliminate trackers and surveillance. Don't willingly share
    your data with services that don't respect you.
  </figcaption>
</figure>
</div>

# Documentation coming soon...
