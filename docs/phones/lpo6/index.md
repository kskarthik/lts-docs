# Liberated Phone - LPO6

<div>
<figure>
  <img src="/images/phones/lp06-pmos.jpg" width="300"/>
  <figcaption>
    Use MORE Free Software on your phone. Opt-out of proprietary
    software. Eliminate trackers and surveillance. Don't willingly share
    your data with services that don't respect you.
  </figcaption>
</figure>
</div>

## Supported Operating Systems

- Lineage OS – [https://wiki.lineageos.org/devices/enchilada/](https://wiki.lineageos.org/devices/enchilada/)
- Ubuntu Touch – [https://devices.ubuntu-touch.io/device/enchilada](https://devices.ubuntu-touch.io/device/enchilada)
- PostmarketOS – [https://wiki.postmarketos.org/wiki/OnePlus*6*(oneplus-enchilada)](<https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada)>)

## Images

<style>
img {
width: 300px;
object-fit: fill;
}
</style>

![ubports](../../images/phones/lp06-ubports.png)
![pmOS](../../images/phones/lp06-pmos.jpg)
