## Login

The router you recieve is pre-flashed with OpenWrt Firmware. You can access the admin interface on [http://192.168.1.1](http://192.168.1.1) from your web browser.

The default user is `root`, leave password blank & click on the log-in button

<div style="align: center">
<figure>
  <img src="/images/routers/openwrt_admin_ui.png" width="1000" align="center" />
  <small>Login Page</small>
</figure>
</div>

## Available Ports

<div style="align: center">
<figure>
  <img src="/images/routers/tplink_back.png" width="1000" align="center" />
  <small>(Source: TPLink's Website)</small>
</figure>
</div>
