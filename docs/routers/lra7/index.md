# Liberated Router: LRA7

<div style="align: center">
<figure>
  <img src="/images/routers/lra7.png" width="420" align="center" />
  <figcaption>
    Build networks powered by Free Software. Be in control. Choose
    greater privacy and security for your network. Do things you've never
    done before.
  </figcaption>
</figure>
</div>

## Introduction to OpenWrt

Description from the [OpenWrt website](https://openwrt.org):

> The OpenWrt Project is a Linux operating system targeting embedded devices. Instead of trying to create a single, static firmware, OpenWrt provides a fully writable filesystem with package management. This frees you from the application selection and configuration provided by the vendor and allows you to customize the device through the use of packages to suit any application. For developers, OpenWrt is the framework to build an application without having to build a complete firmware around it; for users this means the ability for full customization, to use the device in ways never envisioned.

## Hardware Details

The LRA7 is based on TPLink Archer A7 model.

**These are the following specs:**

- CPU: Qualcomm Atheros QCA9563 @ 720MHz
- RAM: 128 MB
- ROM: 16MB
- Dual Band WiFi: 2.5GHz & 5GHz

## Useful Links

- [OpenWrt Page](https://openwrt.org/toh/tp-link/archer_a7_v5?s[]=tp&s[]=link&s[]=archer&s[]=a7&s[]=v5)
