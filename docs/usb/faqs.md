# Troubleshooting & FAQs

## Boot Issues

### Help! The MH-USB won't boot!

First don't panic! :) 

Try out the following steps:

  1. If you insert the USB disk into a computer, does it show up?

     A quick way to test this out on a GNU/Linux system would be to run `lsblk`
     or check `dmesg`.

  2. If the USB does not get detected, then it could either be an issue with
     your USB disk. (Or, at times, the USB port / hub.) See if it gets detected
     in other computers. Or change the USB port. Or try to use it without a USB
     hub (if you're using one).

     Still nothing? Do write in to us at <guide@mostlyharmless.io> with
     "MH-USB" somewhere in the email subject and we'll help you out.

     The [MH-USB discussion forum](https://ask.libre.support/c/usb/) is another
     option.

  3. If it does get detected, then try to mount the partition called "MH-USB".  
     It should get mounted just fine on any OS. If you're able to mount this
     filesystem, check its contents.

  4. If the contents are okay and readable, then its probably a Grub2 issue.  
     Unless you want to debug it and learn about Grub and Ventoy in this
     process, that one option.

     Otherwise, a simple solution might be to just
     backup the contents of the USB disk and re-install the MH-USB software
     using the scripts included with the source code.

### I'm unable to boot operating system X!

That could happen due to multiple reasons:

  1. Its possible that the OS image on the disk has errors in it. The easiest
     way to verify that is to calculate and verify its checksum. 

  2. When using SeaBIOS with Coreboot, it possible that the OS image might not 
     be able to initialise graphics mode and show its internal boot menu. Try
     pressing the ++tab++ key twice to show boot options.

  3. Does Ventoy include support for that operating system? The list of 
     supported (ie. tested out) operating systems is available here:
     <https://www.ventoy.net/en/isolist.html>

If you still face problems, please do post on the [MH-USB discussion 
forum](https://ask.libre.support/c/usb/) so that we can try to debug it 
together.

### I formatted the disk by mistake and now it won't boot!

You can easily recreate the MH-USB on the USB disk (or, for that matter, any 
USB disk out there!) by using the scripts provided with the source code.

## MH-USB Questions

### Can I use it as a portable storage device?

Yes - Sure. The reason the MH-USB partition is formatted as exFAT is so that 
its possible to detect and use it on all operating systems. You can store any 
data on it and use it to transfer files.

!!! warning
    It is not safe to store important or private or sensitive data on a
    portable USB disk. Please be careful. Consider encrypting the storage disk
    if you need to secure the data stored on it.

### Does the MH-USB require upgrades?

Not really. The MH-USB is basically a Grub2 bootable disk which in turn loads 
and boots up operating system images. In case there is a specific bug that 
stops it from working, there isn't any real reason to upgrade Venoty/Grub.

However, if you are interested in contributing to the development effort or 
further customising the MH-USB - that's a good reason to upgrade / update its 
configuration.

### The MH-USB is slow!

Check the [performance benchmarks](/usb/internals/#dd-read-test) for the USB2 
model. Try to replicate them and see if your results come close to it. That 
would indicate a limit of the USB disk's performance.

If you have a USB3 disk and connecting it to a USB2 port, that might result in 
slower transfer speeds as well. Do check that out.

### I really like the 3D printed disk. How do I customise its looks?

Yes sure! By all means. The source code for the 3D printed enclosure is 
included within the MH-USB source code.

If you need help with design, production, customisation or distribution, please 
feel free to reach out.

### Can you send me a USB disk with the OS images I need?

MH-USB variants with custom images would require too much effort - unless what 
you have is a volume requirement (ie. at least 100 units). You can make your 
own custom USB disk very easily by adding the OS images of your choice and 
customising the Grub2 theme.
