# The Mostly Harmless USB disk

<figure style="display: table;">
  <img src="/images/usb/top.png" width="400">
  <figcaption style="display: table-caption; caption-side: bottom;
margin-top: 1em;">
    The answer to the ultimate free software problem: 
    "How do I liberate or fix a computer when I don't always have a bootable USB disk handy?"  
  </figcaption>
</figure>

Details and rationale for the MH-USB: <https://mostlyharmless.io/usb/>

## Screen-share of the MH-USB

<figure>
  <iframe width="560" height="315" sandbox="allow-same-origin
    allow-scripts allow-popups" title="The Mostly Harmless USB Disk - Demo"
    src="https://sovran.video/videos/embed/5f1c871e-d76b-4d26-bcfa-804de1c6fdcb?title=0&warningTitle=0"
    frameborder="0" allowfullscreen></iframe>
</figure>

## Powered by [Ventoy](https://ventoy.net)

The main component of the MH-USB disk is the Ventoy bootloader - derived from Grub2.

<figure style="text-align: center">
  <a href="https://ventoy.net">
    <img src="/images/usb/icons/ventoy.png"> Ventoy
  </a>
</figure>

- Website: <https://ventoy.net>
- Source code: <https://github.com/ventoy/Ventoy>
- Documentation: <https://ventoy.net/en/doc_start.html>
- License: **GPLv3+** - <https://ventoy.net/en/doc_license.html>

## Operating Systems

By default, the following operating systems are available for live boot or
installation:

<hr>

### Alpine

<figure>
  <img alt="image" src="/images/usb/icons/alpine.png"> 
</figure>

!!! tldr "About"
    Alpine Linux is a security-oriented, lightweight Linux distribution
    based on musl libc and busybox.
    
    Alpine Linux is an independent, non-commercial, general purpose
    Linux distribution designed for power users who appreciate security,
    simplicity and resource efficiency.

<figure>
  <img alt="image" src="/images/usb/screenshots/alpine.png"> 
</figure>

- Website: <https://www.alpinelinux.org/> 
- OS Image:
<https://dl-cdn.alpinelinux.org/alpine/v3.15/releases/x86_64/alpine-standard-3.15.0-x86_64.iso> 
- Path on USB: `/OS/alpine-standard-3.15.0-x86_64.iso`
- Type: **Live + Installer** 

<hr>

### Debian GNU/Linux

<figure>
  <img alt="image" src="/images/usb/icons/debian.png"> 
</figure>

!!! tlr "About"
    Debian is a complete Free Operating System! Also known as Debian
    GNU/Linux, it is a GNU/Linux distribution composed of free and
    open-source software.

<figure>
  <img alt="image" src="/images/usb/screenshots/debian.png"> 
</figure>

- Website: <https://www.debian.org>
- OS Image:
  <https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-11.2.0-amd64-cinnamon.iso>
- Path on USB: `/OS/debian-live-11.2.0-amd64-cinnamon.iso`
- Type: **Live + Installer** 

<hr>

### Devuan GNU+Linux

<figure>
  <img alt="image" src="/images/usb/icons/devuan.png">
</figure>

!!! tldr "About"
    Devuan GNU+Linux is a fork of Debian without systemd that allows
    users to reclaim control over their system by avoiding unnecessary
    entanglements and ensuring Init Freedom.

<figure>
  <img alt="image" src="/images/usb/screenshots/devuan.png"> 
</figure>

- Website: <https://www.devuan.org>
- OS Image:
  <https://mirror.ungleich.ch/mirror/devuan/devuan_chimaera/desktop-live/devuan_chimaera_4.0.0_amd64_desktop-live.iso>
- Path on USB: `/OS/devuan_chimaera_4.0.0_amd64_desktop-live.iso`
- Type: **Live + Installer** 

<hr>

### Fedora

<figure>
  <img alt="image" src="/images/usb/icons/fedora.png">
</figure>

!!! tldr "About"
    Fedora Workstation is a polished, easy to use operating system for
    laptop and desktop computers, with a complete set of tools for
    developers and makers of all kinds. 

<figure>
  <img alt="image" src="/images/usb/screenshots/fedora.png"> 
</figure>

- Website: <https://www.getfedora.org>
- OS Image: <https://download.fedoraproject.org/pub/fedora/linux/releases/35/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-35-1.2.iso>
- Path on USB: `/OS/Fedora-Workstation-Live-x86_64-35-1.2.iso` 
- Type: **Live + Installer** 

<hr>

### GNU Guix

<figure>
  <img alt="image" src="/images/usb/icons/guix.png"> 
</figure>

!!! tldr "About"
    Guix is an advanced distribution of the GNU operating system
    developed by the GNU Project—which respects the freedom of computer
    users.
    
    Guix supports transactional upgrades and roll-backs,
    unprivileged package management, and more. When used as a standalone
    distribution, Guix supports declarative system configuration for
    transparent and reproducible operating systems.

<figure>
  <img alt="image" src="/images/usb/screenshots/guix.png"> 
</figure>

- Website: <https://guix.gnu.org>
- OS Image:
  <https://ftp.gnu.org/gnu/guix/guix-system-install-1.3.0.x86_64-linux.iso>
- Path on USB: `/OS/guix-system-install-1.3.0.x86_64-linux.iso`
- Type: **Live + Installer** 

<hr>

### Haiku

<figure>
>   <img alt="image" src="/images/usb/screenshots/haiku-boot.png"> 
</figure>

!!! tldr "About"
    Haiku is an open-source operating system that specifically targets
    personal computing. Inspired by the BeOS, Haiku is fast, simple to use,
    easy to learn and yet very powerful. 
    
    Specifically targeting personal computing, Haiku is a fast, efficient,
    simple to use, easy to learn, and yet very powerful system for computer
    users of all levels. 
    
    Additionally, Haiku offers something over other
    open source platforms which is quite unique: The project consists of a
    single team writing everything from the kernel, drivers, userland
    services, tool kit, and graphics stack to the included desktop
    applications and preflets.

<figure>
  <img alt="image" src="/images/usb/screenshots/haiku.png"> 
</figure>

- Website: <https://www.haiku-os.org/>
- OS Image:
  <https://cdn.haiku-os.org/haiku-release/r1beta3/haiku-r1beta3-x86_64-anyboot.iso>
- Path on USB: `/OS/haiku-r1beta3-x86_64-anyboot.iso` 
- Type: **Live + Installer** 

<hr>

### LibreELEC

<figure>
<img alt="image" src="/images/usb/icons/libreelec.png"> 
</figure>

!!! tldr "About"
    LibreELEC is a lightweight ‘Just enough OS’ Linux distribution
    purpose-built for Kodi on current and popular mediacentre hardware.
    
    Kodi is a media center and entertainment hub that brings all your
    digital media together into a beautiful and user friendly package. It is
    100% free and open source, very customisable and runs on a wide variety
    of devices. It is supported by a dedicated team of volunteers and a huge
    community.

- Website: <https://libreelec.tv/>
- OS Image:
  <https://releases.libreelec.tv/LibreELEC-Generic.x86_64-10.0.1.img.gz>
- Path on USB: `/OS/LibreELEC-Generic.x86_64-10.0.1.img`
- Type: **Live + Installer** 

<hr>

### Manjaro

<figure>
  <img alt="image" src="/images/usb/icons/manjaro.png">
</figure>

!!! tldr "About"
    Manjaro is a user-friendly Linux distribution based on the
    independently developed Arch operating system. Within the Linux
    community, Arch itself is renowned for being an exceptionally fast,
    powerful, and lightweight distribution that provides access to the very
    latest cutting edge - and bleeding edge - software. 

    Developed in Austria, France, and Germany, Manjaro provides all the
    benefits of the Arch operating system combined with a focus on
    user-friendliness and accessibility. Manjaro follows Archlinux and
    officially only offers a 64 bit version. Manjaro is suitable for
    newcomers as well as experienced Linux users.

<figure>
  <img alt="image" src="/images/usb/screenshots/manjaro.png"> 
</figure>

- Website: <https://manjaro.org/>
- OS Image:
  <https://download.manjaro.org/kde/21.2.1/manjaro-kde-21.2.1-220103-linux515.iso>
- Path on USB: `/OS/manjaro-kde-21.2.1-220103-linux515.iso`
- Type: **Live + Installer** 

<hr>

### netboot.xyz

<figure>
  <img alt="image" src="/images/usb/icons/netboot.png"> 
</figure>

!!! tldr "About"
    netboot.xyz lets you PXE boot various operating system installers or
    utilities from a single tool over the network. This lets you use one
    media for many types of operating systems or tools. The iPXE project is
    used to provide a user friendly menu from within the BIOS that lets you
    easily choose the operating system you want along with any specific
    types of versions or bootable flags.

    You can remote attach the ISO to servers, set it up as a rescue option
    in Grub, or even set up your home network to boot to it by default so
    that it's always available.

<figure>
  <img alt="image" src="/images/usb/screenshots/netboot.png"> 
</figure>

- Website: <https://netboot.xyz>
- OS Image: <https://boot.netboot.xyz/ipxe/netboot.xyz.iso>
- Path on USB: `/OS/netboot.xyz.iso`
- Type: **Live + Installer** 
  
<hr>

### Open Media Vault

<figure>
  <img alt="image" src="/images/usb/icons/openmediavault.png" width="250px">
</figure>

!!! tldr "About"
    openmediavault is the next generation network attached storage (NAS)
    solution based on Debian Linux. It contains services like SSH, (S)FTP,
    SMB/CIFS, DAAP media server, RSync, BitTorrent client and many more.
    Thanks to the modular design of the framework it can be enhanced via
    plugins.

<figure>
  <img alt="image" src="/images/usb/screenshots/openmediavault.png"> 
</figure>

- Website: <https://www.openmediavault.org/> 
- OS Image:
  <https://sourceforge.net/projects/openmediavault/files/5.6.13/openmediavault_5.6.13-amd64.iso>
- Path on USB: `/OS/openmediavault_5.6.13-amd64.iso`
- Type: **Installer** 

<hr>

### OpenWRT

<figure>
  <img alt="image" src="/images/usb/icons/openwrt.png">
</figure>

!!! tldr "About"
    The OpenWrt Project is a Linux operating system targeting embedded
    devices. Instead of trying to create a single, static firmware, OpenWrt
    provides a fully writable filesystem with package management. This frees
    you from the application selection and configuration provided by the
    vendor and allows you to customize the device through the use of
    packages to suit any application.

<figure>
  <img alt="image" src="/images/usb/screenshots/openwrt.png"> 
</figure>

- Website: <https://openwrt.org/>
- OS Image:
  <https://downloads.openwrt.org/releases/21.02.0/targets/x86/64/openwrt-21.02.0-x86-64-generic-ext4-combined.img.gz>
- Path on USB: `/OS/openwrt-21.02.0-x86-64-generic-ext4-combined.img`
- Type: **Live + Installer** 

<hr>

### Proxmox VE

<figure>
  <img alt="image" src="/images/usb/icons/proxmox.png"> 
</figure>

!!! tldr "About"
    Proxmox VE is a complete, open-source server management platform for
    enterprise virtualization. It tightly integrates the KVM hypervisor and
    Linux Containers (LXC), software-defined storage and networking
    functionality, on a single platform. With the integrated web-based user
    interface you can manage VMs and containers, high availability for
    clusters, or the integrated disaster recovery tools with ease.

<figure>
  <img alt="image" src="/images/usb/screenshots/proxmox.png"> 
</figure>

- Website: <https://www.proxmox.com/en/proxmox-ve>
- OS Image:
  <https://www.proxmox.com/en/downloads/item/proxmox-ve-7-1-iso-installer>
- Path on USB: `/OS/proxmox-ve_7.1-2.iso` 
- Type: **Installer** 

<hr>

### Qubes OS

<figure>
  <img alt="image" src="/images/usb/icons/qubes.png"> 
</figure>
 
!!! tldr "About"
    Qubes OS is a free and open-source, security-oriented operating
    system for single-user desktop computing. Qubes OS leverages Xen-based
    virtualization to allow for the creation and management of isolated
    compartments called qubes. 

<figure>
  <img alt="image" src="/images/usb/screenshots/qubes.png"> 
</figure>

- Website: <https://www.qubes-os.org/>
- OS Image:
  <https://mirrors.edge.kernel.org/qubes/iso/Qubes-R4.0.4-x86_64.iso>
- Path on USB: `/OS/Qubes-R4.0.4-x86_64.iso`
- Type: **Installer** 

<hr>

### RaspberryPi OS

<figure>
  <img alt="image" src="/images/usb/icons/raspios.png"> 
</figure>

!!! tldr "About"
    Your Raspberry Pi needs an operating system to work. This is it.
    Raspberry Pi OS (previously called Raspbian) is the official supported
    operating system.

- Website: <https://www.raspberrypi.com/>
- OS Image:
  <https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf-lite.zip>
- Path on USB: `/RPi/2021-10-30-raspios-bullseye-armhf-lite.zip`
 
<hr>

### System Rescue

<figure>
  <img alt="image" src="/images/usb/icons/systemrescue.png">
</figure>

!!! tldr "About"
    SystemRescue (formerly known as SystemRescueCd) is a Linux system
    rescue toolkit available as a bootable medium for administrating or
    repairing your system and data after a crash. It aims to provide an easy
    way to carry out admin tasks on your computer, such as creating and
    editing the hard disk partitions. It comes with a lot of Linux system
    utilities such as GParted, fsarchiver, filesystem tools and basic tools
    (editors, midnight commander, network tools). It can be used for both
    Linux and windows computers, and on desktops as well as servers. This
    rescue system requires no installation as it can be booted from a CD/DVD
    drive or USB stick, but it can be installed on the hard disk if you
    wish. The kernel supports all important file systems (ext4, xfs, btrfs,
    vfat, ntfs), as well as network filesystems such as Samba and NFS.

<figure>
  <img alt="image" src="/images/usb/screenshots/systemrescue.png"> 
</figure>

- Website: <https://www.system-rescue.org/>
- OS Image:
  <https://sourceforge.net/projects/systemrescuecd/files/sysresccd-x86/9.00/systemrescue-9.00-amd64.iso/download>
- Path on USB: `/OS/systemrescue-8.06-amd64.iso`
- Type: **Live** 

<hr>

### Tails

<figure>
  <img alt="image" src="/images/usb/icons/tails.png">
</figure>

!!! tldr "About"
    Tails is a portable operating system
    that protects against surveillance and censorship.

<figure>
  <img alt="image" src="/images/usb/screenshots/tails.png"> 
</figure>

- Website: <https://tails.boum.org/>
- OS Image:
  <https://mirrors.edge.kernel.org/tails/stable/tails-amd64-4.26/tails-amd64-4.26.img>
- Path on USB: `/OS/tails-amd64-4.25.img`
- Type: **Live + Installer** 

<hr>

### Trisquel GNU/Linux

<figure>
  <img alt="image" src="/images/usb/icons/trisquel.png">
</figure>

!!! tldr "About"
    Trisquel GNU/Linux is a fully free operating system for home users,
    small enterprises and educational centers.

<figure>
  <img alt="image" src="/images/usb/screenshots/trisquel.png"> 
</figure>

- Website: <https://trisquel.info/>
- OS Image: <https://trisquel.info/en/download>
- Path on USB: `/OS/trisquel-mini_9.0.1_amd64.iso`
- Type: **Live + Installer** 

<hr>

### Ubuntu

<figure>
  <img alt="image" src="/images/usb/icons/ubuntu.png">
</figure>

!!! tldr "About"
    Ubuntu comes with everything you need to run your organisation,
    school, home or enterprise. All the essential applications, like an
    office suite, browsers, email and media apps come pre-installed and
    thousands more games and applications are available in the Ubuntu
    Software Centre.

<figure>
  <img alt="image" src="/images/usb/screenshots/ubuntu.png"> 
</figure>

- Website: <https://ubuntu.com/>
- OS Image:
  <https://releases.ubuntu.com/20.04.3/ubuntu-20.04.3-desktop-amd64.iso>
- Path on USB: `/OS/ubuntu-20.04.3-desktop-amd64.iso`
- Type: **Live + Installer** 

<hr>

## Tools & Programs

### The Raspberry Pi Imager

<figure>
  <img alt="image" src="/images/usb/screenshots/rpi-imager.png"> 
</figure>

!!! tldr "About"
    The Raspberry Pi Imager is the quick, safe and easy way to install an
    operating systems to a microSD card or USB disk.

- Website: <https://www.raspberrypi.com/software/>
- Image: <http://downloads.raspberrypi.org/imager/imager_amd64.AppImage>
- Path on USB: `/Tools/imager_amd64.AppImage`

### Ungoogled Chromium

!!! tldr "About"
    ungoogled-chromium is Google Chromium, sans dependency on Google web
    services. It also features some tweaks to enhance privacy, control, and
    transparency (almost all of which require manual activation or
    enabling).

    ungoogled-chromium retains the default Chromium experience as closely as
    possible. Unlike other Chromium forks that have their own visions of a
    web browser, ungoogled-chromium is essentially a drop-in replacement for
    Chromium.

    ungoogled-chromium addresses these issues in the following ways:

    - Remove all remaining background requests to any web services while building and running the browser
    - Remove all code specific to Google web services
    - Remove all uses of pre-made binaries from the source code, and replace them with user-provided alternatives when possible.
    - Disable features that inhibit control and transparency, and add or
      modify features that promote them (these changes will almost always
      require manual activation or enabling).

<figure>
  <img alt="image" src="/images/usb/screenshots/ungoogled-chromium.png"> 
</figure>

- Website: <https://ungoogled-software.github.io/>
- Image:
  <https://ungoogled-software.github.io/ungoogled-chromium-binaries/>
- Path on USB: `/Tools/ungoogled-chromium_91.0.4472.164-1.1.AppImage`

