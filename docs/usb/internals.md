# MH-USB Internals

## MH-USB Performance

### USB2 Version

From <https://www.blackmoreops.com/2017/04/04/usb-and-ssd-drive-speedtest-in-linux/>

> USB 2.0 has a theoretical maximum signaling rate of 480 Mbits/s or 60
> Mbytes/s. However due to various constraints the maximum throughput is
> restricted to around 280 Mbit/s or 35 Mbytes/s.

#### `hdparm` tests

``` console
$ sudo hdparm -Ttv /dev/sde1
/dev/sde1:
 multcount     =  0 (off)
 readonly      =  0 (off)
 readahead     = 256 (on)
 geometry      = 15600/64/32, sectors = 31881176, start = 2048
 Timing cached reads:   16422 MB in  2.00 seconds = 8221.42 MB/sec
 Timing buffered disk reads:  64 MB in  3.01 seconds =  21.28 MB/sec
```

#### `dd` write test

``` console
$ dd if=/dev/zero of=perf oflag=direct bs=128k count=8k
8192+0 records in
8192+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 6.94198 s, 155 MB/s
```

#### `dd` read test

``` console
## Clear the memory cache before benchmarking reads
$ sudo sh -c "sync && echo 3 > /proc/sys/vm/drop_caches"
$ dd if=perf of=/dev/null bs=4k
262144+0 records in
262144+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 44.5847 s, 24.1 MB/s
```
### USB3 Version

*Coming soon!*

## The MH-USB partition layout

### The Ventoy partion layout

*Document how Ventoy creates partitions for both GPT and MBR partition tables*

### The 16 GB model

*Standard ventoy partitions*

### The 32 GB model

*Coming soon! (special partitions to accommodate haiku.)*

## Using the MH-USB Source Code

### Overview of the source code

### The scripts

#### `download-images.sh`

#### `prepare-usb.sh` 

#### `qemu-*.sh`

### Installing MH-USB

### Customising the MH-USB 
