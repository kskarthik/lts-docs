# Adding / Updating Images on the MH-USB Disk

## How do OS images get listed on the boot screen?

Recall the MH-USB directory layout:

```
MH-USB/
       /OS/      <-- Contains OS Images
       /Tools/   <-- Contains some tools and software
       /RPi/     <-- OS Images for the Raspberry Pi
       /ventoy/  <-- Config files for the Ventoy/Grub2 bootloader
```

While the operating system images are stored in the `/OS/` directory in the
root of the USB disk, the actual display and order of display is controlled by
the ventoy config file: `/ventoy/ventoy.json`.

An excerpt from the `ventoy.json` config file:

``` json
{
    "menu_alias":[
        {
            "image": "/OS/debian-live-11.2.0-amd64-cinnamon.iso",
            "alias": "Debian 11.2.0 (Live)"
        },
        {
            "image": "/OS/devuan_chimaera_4.0.0_amd64_desktop-live.iso",
            "alias": "Devuan Chimaera 4.0.0"
        },
        {
            "image": "/OS/guix-system-install-1.3.0.x86_64-linux.iso",
            "alias": "Guix System Installer 1.3.0"
        },
        {
            "image": "/OS/LibreELEC-Generic.x86_64-10.0.1.img",
            "alias": "LibreELEC 10.0.1 (Live)"
        }
    ]
}
```

The image would get listed on the boot screen in the alphabetical order even
without this configuration. However, in the absence of this, the file name of
the ISO image would show up instead of a more useful OS description.

## Adding, updating and removing a OS image

### Removing an image

Don't think you're likely to use an OS image that exists on the USB
disk? No problem. Just go ahead and delete it! It will shop showing up
on the boot screen and you will also have some free space on the USB
drive.

### Adding a new image

You can copy a new bootable OS image anywhere on the USB disk. You could
copy it to the `/OS/` directory to keep things neat. But that's not
mandatory. You can copy it anywhere.

### Updating an image

Updating an OS image simply the process of deleting an older image file
and adding the new one.

### Update the Ventoy configuration

If you would like to see a proper descriptive name for your new or
updated OS image, then you can add or update the corresponding section
in the `/ventoy/ventoy.json` file.

