# Using the MH-USB Disk

## Organisation of the MH-USB Disk

The files on the MH-USB disk are arranged as follows:

```
MH-USB/
       /OS/      <-- Contains OS Images
       /Tools/   <-- Contains some tools and software
       /RPi/     <-- OS Images for the Raspberry Pi
       /ventoy/  <-- Config files for the Ventoy/Grub2 bootloader
```

The MH-USB partition of the USB disk is formatted with [exFAT](https://en.wikipedia.org/wiki/ExFAT).
This enables universal access to the data on any operating system that supports exFAT.

## Booting on physical computer

You boot the USB disk on a physical computer (dekstop, laptop, single-board
computer or laptop) by simply plugging it in and choosing it as a boot device.

The MH-USB runs the [GRUB2](https://www.gnu.org/software/grub/) boot
loader and will boot just fine with a "legacy" BIOS or UEFI BIOS.

The USB make would show up as a USB disk called "Generic Flash Drive":

<figure>
  <img src="/images/usb/boot-device.jpg" width="600">
</figure>

Just choose it as the boot device and you will be greeted by the MH-USB
boot screen.

## Booting on a virtual machine

You can also boot the USB using a virtual machine. On a GNU/Linux
system, one can use QEMU to boot the USB.

First check the device name assigned to the USB disk. A quick way to
check it out would be to use the `lsblk` command like this:

``` shell-session
$ lsblk -o NAME,SIZE,HOTPLUG -P -d | grep "HOTPLUG=\"1\"" | cut -d" " -f1
NAME="sdb"  
```

The command above will list out all the USB disks attached to your
computer. (Actually, all "HOTPLUG" storage devices - which includes USB
storage.)

### Booting a virtual machine with SeaBIOS

To boot the USB disk using QEMU + SeaBIOS, install the `qemu` package
from your GNU/Linux OS repository and then run:

``` shell-session
$ DEVICE=`lsblk -o NAME,SIZE,HOTPLUG -P -d | grep "HOTPLUG=\"1\"" | cut -d"\"" -f2`
$ sudo qemu-system-x86_64 \
    -enable-kvm -rtc base=localtime -m 2G -vga std \
    -drive file=/dev/$DEVICE,readonly=on,cache=none,format=raw,if=virtio
```

This will boot the USB via QEMU and show you the boot screen in a popup window.

### Booting a virtual machine with a UEFI BIOS

If you want to try booting the USB with QEMU with the `tianocore` UEFI BIOS, first install the OVMF
(Open Virtual Machine Firmware) packages.

On Debian and Debian-based distributions run:

``` shell-session
# apt-get install ovmf
$ BIOS="/usr/share/qemu/OVMF.fd"
```

On Archlinux-based distributions run:

``` shell-session
# pacman -S edk2-ovmf
$ BIOS="/usr/share/edk2-ovmf/x64/OVMF.fd"
```

```bash
$ DEVICE=`lsblk -o NAME,SIZE,HOTPLUG -P -d | grep "HOTPLUG=\"1\"" | cut -d"\"" -f2`
$ sudo qemu-system-x86_64 \
    -bios $BIOS \
    -enable-kvm -rtc base=localtime -m 2G -vga std \
    -drive file=/dev/$DEVICE,readonly=on,cache=none,format=raw,if=virtio
``` 

## Writing an image to another USB drive

While a multi-boot USB is nice, there might be times when you want to write an OS image to another
USB disk. The MH-USB includes the "RaspberryPi Imager" (`rpi-imager`) tool to help you write an OS
image to a USB disk safely and quickly.

You can run the the `rpi-imager` directly from the USB. Insert the MH-USB into a GNU/Linux (or
Windows) computer and mount it. In the `Tools/` directory on the USB, you will see binaries for the
Raspberry Pi imager. You can use it to write any of the bootable operating system images to another
USB.

*[TODO: Include more details and screenshots!]*

## Working with Raspberry Pi images

*[TODO: Include details with screenshots]*
