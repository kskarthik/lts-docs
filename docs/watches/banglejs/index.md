# Bangle.js

Bangle.js is an open source hackable smart watch

From the Banglejs Website:

> You can easily install new apps from the web or develop your own using JavaScript or a graphical programming language (Blockly). All you need is a Web Browser (Chrome, Edge or Opera) and you can upload apps or write code to run on your watch wirelessly! Bangle.js is water resistant, AI enabled, and comes with Bluetooth Low Energy, GPS, a heart rate monitor, accelerometer and more.

## Features

- IP67 Water Resistant (everyday use, not swimming or baths)
- Nordic 64MHz nRF52840 ARM Cortex-M4 processor with Bluetooth LE
- 256kB RAM 1024kB on-chip flash, 8MB external flash (GD25Q64C/E)
- 1.3 inch 176x176 always-on 3 bit colour LCD display (LPM013M126)
- Full touchscreen
- GPS/Glonass receiver (AT6558)
- Heart rate monitor (Vcare VC31 / VC31B)
- 3 Axis Accelerometer (Kionix KX023)
- 3 Axis Magnetometer
- Air Pressure/Temperature sensor (Bosch BMP280 / Goertek SPL06)
- Vibration motor
- 200mAh battery, 4 weeks standby time
- 36mm x 43mm x 12mm watch body, with standard 20mm watch straps

## Images

<style>
img {
width: 300px;
object-fit: fill;
}
</style>

![Main Image](../../images/watches/banglejs-1.jpg)
![Main Image](../../images/watches/banglejs-casio.jpg)
![Main Image](../../images/watches/banglejs-backlight.jpg)

## Useful Links

- [Official Website](https://banglejs.com/)

- [Technical Specifications & User manual](https://www.espruino.com/Bangle.js2)

- [API Documentation](https://www.espruino.com/Reference#software)
