# Liberated Watch LWZ2

This watch uses [AsteroidOS](https://asteroidos.org/), which is a free and open source operating system for smart watches

From the Asteroid OS Website:

> AsteroidOS is built upon a rock-solid base system. Qt and QML are used for fast and easy app development. OpenEmbedded provides a full GNU/Linux distribution and libhybris allows easy porting to most Android and Wear OS watches.

<style>
img {
width: 300px;
object-fit: fill;
}
</style>

## Images

![Main Image](../../images/watches/09.png)
![Main Image](../../images/watches/10.png)
![Main Image](../../images/watches/01.png)
![Main Image](../../images/watches/02.png)
![Main Image](../../images/watches/07.png)
![Main Image](../../images/watches/08.png)
![Main Image](../../images/watches/03.png)
![Main Image](../../images/watches/04.png)
![Main Image](../../images/watches/06.png)
![Main Image](../../images/watches/05.png)
